package main
import "fmt"
import "errors"

// no try catch finally block in go
// builtin Error type
// type error interface {
//    Error() string
// }

type MyError string

func (e MyError) Error() string {
  return string(e)
}

func foo() error {
  return errors.New("Some Error Occured")
}

func main() {
  if err := foo(); err != nil {
    fmt.Println("error :'-[")
    fmt.Println(err)
  }
}
