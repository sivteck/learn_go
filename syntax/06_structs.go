package main
import "fmt"

// Structs in Go dont have scoping modifiers
// types and fields that begin with capital letter are exported and accessible outside the package
// types and fields that begin with lowercase letter are private and accessible only within the package

type Person struct {
  Name string
  Age int
}

func (p *Person) SayHello() {
  fmt.Println("Hello ", p.Name)
}

func main() {
  guy := new (Person)
  guy.Name = "siva"
  guy.SayHello()
}
