package main
import "fmt"

func main() {
  var count = int(5)
  ptr := &count
  fmt.Println(*ptr)
  *ptr = 1000
  fmt.Println(count)
}
