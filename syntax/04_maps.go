package main
import "fmt"

// Maps are associative arrays, unordered list of key/value pairs

func main() {
  m := make(map[string]string)
  m["some key"] = "monkey"
  fmt.Println(m)
}
