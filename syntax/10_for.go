package main
import "fmt"

// there is no while loop in Go, only for, deal with it

func main() {
  for i := 0; i < 10; i ++ {
    fmt.Println(i)
  }

// for looping over a collection(slicer or maps)
  nums := []int{1,2,10,100,200,292}
  for idx, val := range nums {
    fmt.Println(idx, val)
  }
}
