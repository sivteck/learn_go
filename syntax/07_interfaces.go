package main
import "fmt"

//Go interface is a blueprint or contract that defines an expected set
//of actions that any concrete implementation must fulfill in order to be
//considered a type of that interface
//
//An interface consists of a set of methods:
//  Any type that implements those methods with the corresponding signature is considered
//  as a type of that interface


type Friend interface {
  SayHello()
}

// function
func Greet (f Friend) {
  f.SayHello()
}

type Person struct {
  Name string
  Age int
}

// method on Person type
func (p *Person) SayHello() {
  fmt.Println("Hello ", p.Name)
}


func main() {
  var guy = new(Person)
  guy.Name = "sivan"
  guy.Age = 8
  Greet(guy)
}
