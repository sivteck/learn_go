package main
import "fmt"

// slices are arrays that can be dynamically resized and passed to functions more efficiently

func main() {
  var s = make([]string, 0)
  s = append(s, "some string")
  s = append(s, " some other stringy")
  fmt.Println(s)
}
