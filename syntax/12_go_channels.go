package main
import "fmt"

// channels are used to synchronize execution and communication between goroutines

func strlen(s string, c chan int) {
  c <- len(s)
}

func main() {
  c := make(chan int)
  go strlen("Salutations", c)
  go strlen("World", c)
  x, y := <-c, <-c
  fmt.Println(x, y, x+y)
}
