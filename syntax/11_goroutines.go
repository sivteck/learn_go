package main
import "fmt"
import "time"

func f() {
  fmt.Println("just for the sake of printing inside f function")
}

func main() {
  // go keyword before a method or a function call to run it concurrently
  go f()
  fmt.Println("something printed in main function after go f()")
  time.Sleep(1 * time.Second)
  fmt.Println("main function")
}
