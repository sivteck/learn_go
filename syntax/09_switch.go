package main
import "fmt"

// i.(type) to get the type

func typeSwitch (i interface{}) {
  switch v := i.(type) {
  case int:
    fmt.Println("le int ", v)
  case string:
    fmt.Println("le string ", v)
  default:
    fmt.Println("sorry dk ", v)
  }
}

func main() {
  typeSwitch("meme kk")
  typeSwitch(1202)
}
